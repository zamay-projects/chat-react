const { UserRepository } = require('../repositories/userRepository');

class UserService {
    getAll() {
        const items = UserRepository.getAll();
        if (!items) {
            throw Error('No users found');
        }
        return items;
    }

    getAllLength() {
        const items = UserRepository.getAllLength();
        if (!items) {
            throw Error('No users found');
        }
        return items;
    }

    getOne(id) {
        const item = UserRepository.getOne(id);
        if(!item){
            throw Error('User doesn\'t exist.');
        }
        return item;
    }

    create(userData) {
        const ifUserExist = UserRepository.getOne({
            username: userData.username
        });
        if (!ifUserExist) {
            const item = UserRepository.create(userData);
            if (!item) {
                throw Error('Server error');
            }
            // delete item.id;
            return item;
        } else {
            throw Error('User already exists');
        }
    }

    update(id, user) {
        const item = UserRepository.update(id, user);
        if (!item) {
            throw Error('Can not update user');
        }
        delete item.id;
        return item;
    }
    delete(id) {
        const item = UserRepository.delete(id);
        if (!item.length) {
            throw Error('No such user');
        }
        return item;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if (!item) {
            throw Error('No such user found');
        }
        item.result = true;
        item.isAdmin = (item.id === '1');
        return item;
    }
}

module.exports = new UserService();
