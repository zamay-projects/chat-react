const { MessageRepository } = require('../repositories/messageRepository');

class MessageService {
    getAll() {
        const items = MessageRepository.getAll();
        if (!items) {
            throw Error('No messages found');
        }
        return items;
    }

    getAllLength() {
        const items = MessageRepository.getAllLength();
        if (!items) {
            throw Error('No messages found');
        }
        return items;
    }

    getOne(id) {
        const item = MessageRepository.getOne(id);
        if(!item){
            throw Error('Message doesn\'t exist.');
        }
        return item;
    }

    create(messageData) {
        console.log(messageData);
        const item = MessageRepository.create(messageData);
        if (!item) {
            throw Error('Server error');
        }
        return item;
    }

    update(id, message) {
        const item = MessageRepository.update(id, message);
        if (!item) {
            throw Error('Can not update message');
        }
        return item;
    }

    delete(id) {
        const item = MessageRepository.delete(id);
        if (!item.length) {
            throw Error('No such message');
        }
        return item;
    }
}

module.exports = new MessageService();
