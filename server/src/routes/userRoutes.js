const { Router } = require('express');
const UserService = require('../services/userService');
// const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get('/', async (req, res, next) => {
  try {
    res.data = await UserService.getAll();
  } catch (error) {
    res.err = error;
  } finally {
    next();
  }
}, responseMiddleware);

router.get('/length', async (req, res, next) => {
  try {
    res.data = await UserService.getAllLength();
  } catch (error) {
    res.err = error;
  } finally {
    next();
  }
}, responseMiddleware);

router.get('/:id', async (req, res, next) => {
  try {
    res.data = await UserService.getOne({id: req.params.id});
  } catch (error) {
    res.err = error;
  } finally {
    next();
  }
}, responseMiddleware)

router.post('/',  async (req, res, next) => {
  if(res.err){
    next();
    return;
  }

  try {
    res.data = await UserService.create(req.body);
  } catch (error) {
    res.err = error;
  } finally {
    next();
  }
}, responseMiddleware);

router.put('/:id', async (req, res, next) => {
  if(res.err){
    next();
    return;
  }

  try {
    res.data = UserService.update(req.params.id, req.body);
  } catch (error) {
    res.err = error;
  } finally {
    next();
  }
}, responseMiddleware)

router.delete('/:id', async (req, res, next) => {
  try {
    res.data = await UserService.delete(req.params.id);
  } catch (error) {
    res.err = error;
  } finally {
    next();
  }
}, responseMiddleware)


module.exports = router;
