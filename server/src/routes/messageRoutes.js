const { Router } = require('express');
const MessageService = require('../services/messageService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get('/', async (req, res, next) => {
  try {
    res.data = await MessageService.getAll();
  } catch (error) {
    console.log(error);
    res.err = error;
  } finally {
    next();
  }
}, responseMiddleware);

router.get('/:id', async (req, res, next) => {
  try {
    res.data = await MessageService.getOne({id: req.params.id});
  } catch (error) {
    res.err = error;
  } finally {
    next();
  }
}, responseMiddleware)

router.get('/length', async (req, res, next) => {
  try {
    res.data = await MessageService.getAllLength();
  } catch (error) {
    res.err = error;
  } finally {
    next();
  }
}, responseMiddleware);

router.post('/',  async (req, res, next) => {
  if(res.err){
    next();
    return;
  }

  try {
    res.data = await MessageService.create(req.body);
  } catch (error) {
    res.err = error;
  } finally {
    next();
  }
}, responseMiddleware);

router.put('/:id', async (req, res, next) => {
  if(res.err){
    next();
    return;
  }

  try {
    res.data = MessageService.update(req.params.id, req.body);
  } catch (error) {
    res.err = error;
  } finally {
    next();
  }
}, responseMiddleware)

router.delete('/:id', async (req, res, next) => {
  try {
    res.data = await MessageService.delete(req.params.id);
  } catch (error) {
    res.err = error;
  } finally {
    next();
  }
}, responseMiddleware)

module.exports = router;
