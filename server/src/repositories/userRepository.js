const { BaseRepository } = require('./baseRepository');

class UserRepository extends BaseRepository {
  constructor() {
    super('users');
  }

  create(data) {
    data.id = this.generateId();
    return this.dbContext.push(data).write();
    // return list.find(it => it.id === data.id);
  }

  //Todo: пропадает id после update
  update(id, dataToUpdate) {
    const user = this.getOne({id})
    user.username = dataToUpdate.username
    return this.dbContext.find({id}).assign(user).write();

  }
}

exports.UserRepository = new UserRepository();
