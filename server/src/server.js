// const users = require("./routes/users");
const express = require('express');
const cors = require('cors');
const app = express();
require('./config/db');


app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const routes = require('./routes/index');
routes(app);

app.use('/', express.static('../../client/build'));

// app.post('/login', (req, res) => {
//   const { username, password } = req.body;
//   if (username === 'admin' && password === 'admin'){
//     res.send({isAdmin: true, result: true, message: 'Welcome ' + username})
//   //  todo: прервать отправку  дальше и можно убрать else
//   }  else {
//     const getUser = users.get(username);
//
//     if (!getUser) {
//       res.send({result: false, message: "User is not found"})
//     } else {
//       const result = getUser['username'] === username && getUser['password'] === password
//       if (result) {
//         res.send({result, message: 'Welcome ' + username})
//         // res.redirect('/');
//       } else {
//         res.send({result, message: 'Invalid password'})
//       }
//     }
//   }
// });


app.listen(3001, () => {
  console.log('running server')
})
