// const { check, validationResult } = require('express-validator');
//
// // Todo: пересмотреть валидацию
// const createUserValid = async (req, res, next) => {
//     const trim = { ignore_whitespace: true };
//     await check('email', 'Enter correct gmail')
//       // .isEmail()
//       // .matches(/[a-zA-Z0-9]+(@gmail.com$)/)
//       .run(req);
//     await check('firstName', 'Enter your name')
//       .notEmpty(trim)
//       .run(req);
//     await check('lastName', 'Enter your surname')
//       .notEmpty(trim)
//       .run(req);
//     await check('password', "Password must be at least 3 characters")
//       .isLength({ min: 3 })
//       .run(req);
//     await check('phoneNumber', 'Enter the number in the format +38XXXXXXXXXX')
//       // .isMobilePhone(['uk-UA'], { strictMode: true })
//       .run(req);
//
//     const errors = validationResult(req);
//     if (!errors.isEmpty()) {
//         let resMsgObj = {};
//         errors.errors.forEach(item => {
//             resMsgObj[item.param] = item.msg;
//         });
//         return res.status(400)
//           .json({
//               error: true,
//               message: resMsgObj
//           });
//     }
//     console.log('3')
//     next();
// }
//
// // function errorValid(errors, res) {
// //     console.log('2')
// //     let resMsgObj = {};
// //     errors.errors.forEach(item => {
// //         resMsgObj[item.param] = item.msg;
// //     });
// //     return res.status(400)
// //       .json({
// //           error: true,
// //           message: resMsgObj
// //       });
// // }
//
// const updateUserValid = async (req, res, next) => {
//     const trim = { ignore_whitespace: true };
//     for (let key in req.body) {
//         if (key === 'email') {
//             await check('email', 'Enter correct gmail')
//               // .isEmail()
//               .matches(/[a-zA-Z0-9]+(@gmail.com$)/)
//               .run(req);
//             continue;
//         } else if (key === 'password') {
//             await check('password', "Password must be at least 3 characters")
//               .isLength({ min: 3 })
//               .run(req);
//             continue;
//         } else if (key === 'firstName' || key === 'lastName') {
//             await check(key, 'Empty field')
//               .notEmpty(trim)
//               .run(req);
//             continue;
//         } else if (key === 'phoneNumber') {
//             await check('phoneNumber', 'Enter the number in the format +38XXXXXXXXXX')
//               .isMobilePhone(['uk-UA'], { strictMode: true })
//               .run(req);
//             continue;
//         } else {
//             return res.status(400)
//               .json({
//                   error: true,
//                   message: 'Forbidden field'
//               });
//         }
//     }
//
//     const errors = validationResult(req);
//     if (!errors.isEmpty()) {
//         let resMsg = '';
//         errors.errors.forEach(msg => resMsg += `${msg.msg}; `);
//         return res.status(400)
//           .json({
//               error: true,
//               message: resMsg
//           });
//     }
//     next();
// }
//
// exports.createUserValid = createUserValid;
// exports.updateUserValid = updateUserValid;
