import { useState } from 'react';
import data from "../data";

export default () => {
  const [messages, setMessages] = useState(data);
  return {
    messages,
    addMessage: message => {
      setMessages([...messages, message]);
    },
    deleteMessage: messageId => {
      const newMessage = messages.filter((msg) => msg.id !== messageId);
      setMessages(newMessage);
    }
  };
};
