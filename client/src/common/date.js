export default (data) => {
  return new Date(data)
    .toLocaleString(undefined, {hour: '2-digit', hour12: false, minute:'2-digit'})
}
