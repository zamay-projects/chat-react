import { setLocalStorageItem, getObjectFromLocalStorage } from "./localStorageHelper";

export const isSignedIn = () => {
    const user = getObjectFromLocalStorage('user');
    return !!user;
}

export const getLoginSession = () => {
    const user = getObjectFromLocalStorage('user');
    return user ? user : null;
}

export const setLoginSession = (user) => {
    setLocalStorageItem('user', user);
}

export const unsetLoginSession = () => {
    setLocalStorageItem('user', null);
}
