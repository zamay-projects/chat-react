import axios from "axios";
const apiUrl = "http://localhost:3001/api/users";

export function getUsers() {
  return axios.get(apiUrl);
}

export function addUser(user) {
  return axios.post(apiUrl, user);
}

export function updateUser(id, username) {
  return axios.put(apiUrl + "/edit/" + id, {username});
}

export function deleteUser(id) {
  return axios.delete(apiUrl + "/" + id);
}
