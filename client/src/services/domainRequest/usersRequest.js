import { get, post, put, deleteReq } from "../requestHelper";

const entity = 'users'

export const getUsers = async () => {
    return await get(entity);
}

export const getUsersLength = async () => {
    return await get(entity, 'length');
}

export const createUser = async (body) => {
    return await post(entity, body);
}

export const updateUser = async (id, body) => {
    return await put(entity, id, body);
}

export const deleteUser = async (id) => {
    return await deleteReq(entity, id);
}
