import {Chat, Login, Users} from './components/common';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import {useState} from "react";
import useToken from "./services/useToken";

const url = 'https://edikdolynskyi.github.io/react_sources/messages.json';

function App() {
  const {token} = useToken();
  const [userIsLogged, setUserIsLogged] = useState(token?.result || false);

  if (!userIsLogged) {
    return <Login setUserIsLogged={setUserIsLogged}/>
  }

  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/">
          <Chat />
        </Route>
        {token?.isAdmin && <Route path="/users" component={Users} />}
      </Switch>
    </BrowserRouter>
  );
}

export default App;
