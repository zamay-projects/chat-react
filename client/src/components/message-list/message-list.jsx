import {Message, OwnMessage, Preloader} from "../common";
import styles from './message-list.css';
import Header from "../header/header";
import * as PropTypes from "prop-types";
import {useEffect, useRef} from "react";

const MessageList = ({isLoaded, messages, deleteMessage, editMessage}) => {
  let lastDateMsg = new Date('1-01-1970');
  const messagesEndRef = useRef(null)

  const scrollToBottom = () => {
    messagesEndRef.current.scrollIntoView({behavior: "smooth"})
  }

  useEffect(scrollToBottom, [messages]);

  const getTime = (message) => {
    const sentMsg = new Date(message.createdAt)
    const getYear = sentMsg.getFullYear();
    const getYearNow = new Date().getFullYear();

    return sentMsg.toLocaleString('ru-RU',
      (getYear < getYearNow ? {
        year: 'numeric',
        month: '2-digit',
        day: '2-digit'
      } : {
        day: '2-digit',
        month: 'long'
      })
    )
  }

  const checkDate = (message) => {
    const msgDate = getTime(message);
    if (msgDate === lastDateMsg) {
      return ''
    } else {
      lastDateMsg = msgDate;

      return (
        <div className="messages-divider">
          <span>{msgDate}</span>
        </div>
      )
    }
  }


  return (
    <main className="message-list">
      {isLoaded ? <Preloader/> : null}
      {messages.map((msg) => {
        if (msg.userId === '1') {
          return (
            <div key={msg.id}>
              {checkDate(msg)}
              <OwnMessage message={msg}
                          key={msg.id}
                          index={msg.id}
                          deleteMessage={deleteMessage}
                          editMessage={editMessage}/>
            </div>
          )
        } else {
          return (
            <div key={msg.id}>
              {checkDate(msg)}
              <Message message={msg} key={msg.id}/>
            </div>
          )
        }
      })}
      <div ref={messagesEndRef}/>
    </main>
  )
}

Header.propTypes = {
  isLoaded: PropTypes.bool,
  messages: PropTypes.array,
  deleteMessage: PropTypes.func
};

export default MessageList;
