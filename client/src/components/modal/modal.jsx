import styles from './modal.css';
import {useEffect, useState} from "react";
import * as PropTypes from "prop-types";

const Modal = ({textMessage, handleUpdate, toggleModal}) => {
  const [value, setValue] = useState(textMessage);
  const [isOpen, setsOpen] = useState(toggleModal);

  const onChange = event => {
    setValue(event.target.value);
  }

  useEffect(() => {
    setsOpen(toggleModal);
    if (textMessage) {
      setValue(textMessage)
    }
  }, [toggleModal])

  const onClose = (e) => {
    e.preventDefault();
    setsOpen(!isOpen);
  }

  return (
    <div className={isOpen ? "edit-message-modal modal-show" : "edit-message-modal"}>
      <div className="edit-message-container">
        <h2>Edit message</h2>
        <form onSubmit= {event => {
          event.preventDefault();
          if (value.trim()){
            handleUpdate(value);
            setValue('');
            onClose(event);
          }
        }}>
          <textarea type="text" className="edit-message-input"
                    value={value || ''}
                    onChange={onChange} />
          <div className="edit-message-group-button">
            <button type="submit" className="edit-message-button" onClick={onsubmit}>Edit</button>
            <button type="click" className="edit-message-close" onClick={onClose}>Close</button>
          </div>
        </form>
      </div>
    </div>
  )
}

Modal.propTypes = {
  inputValue: PropTypes.func,
  textMessage: PropTypes.string,
  toggleModal: PropTypes.bool
};

export default Modal;
