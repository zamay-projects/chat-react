import styles from './message.css';
import PropTypes from "prop-types";
import {useState} from "react";
import Date from "../../common/date"

const Message = ({message}) => {
  const [isLiked, setIsLiked] = useState(false);
  const date = Date(message.editedAt || message.createdAt)

  const onLike = () => {
    setIsLiked(!isLiked)
    message.liked = isLiked;
  }

  return (
    <div className="msg message" key={message?.id}>
      <div className="message-user-avatar">
        <img
          src={message.avatar}
          alt="profile-pic"/>
      </div>

      <div className="message-block">
        <div className="message-info">
          <div className="message-user-name">{message.user}</div>
          <div className="message-time">{date}</div>
          <div className="message-liked" onClick={onLike}>
            <i className={isLiked ? "fas fa-heart liked" : "far fa-heart"} />
          </div>
        </div>
        <div className="message-text">{message.text} </div>
      </div>
    </div>
  )
}

Message.propTypes = {
  id: PropTypes.number,
  user: PropTypes.string,
  avatar: PropTypes.string,
  userId: PropTypes.number,
  text: PropTypes.string,
  createdAt: PropTypes.string,
  editedAt: PropTypes.string
};

export default Message;
