import styles from './message-input.css';
import * as PropTypes from "prop-types";
import useInputState from "../../common/useInputState";

const MessageInput = ({inputValue}) => {
  const { value, reset, onChange } = useInputState();

  return (
    <form
      onSubmit = {event => {
        event.preventDefault();
        if (value.trim()){
          inputValue(value);
          reset();
        }
      }}
      className="message-input">
      <input type="text"
             className="message-input-text"
             placeholder="Enter your message..."
             onChange={onChange}
             value={value}
      />
      <button type="submit" className="message-input-button" onClick={onsubmit}>Send</button>
    </form>
  )
}
MessageInput.propTypes = {
  inputValue:  PropTypes.func
};

export default MessageInput;
