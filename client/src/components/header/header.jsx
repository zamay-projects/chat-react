import * as PropTypes from "prop-types";
import styles from './header.css';
import useToken from "../../services/useToken";
import {Link} from 'react-router-dom';
import {useEffect, useState} from "react";
import {getUsersLength} from "../../services/domainRequest/usersRequest";
import {getMessagesLength} from "../../services/domainRequest/messagesRequest";

const Header = ({ lastMessageTime, toggleTheme, onToggleTheme, eventEmitHeader}) => {
  const {logout} = useToken();
  const [userCount, setUserCount] = useState(0);
  const [messageCount, setMessageCount] = useState(0);

  const onLogout = () => {
    logout()
    window.location.reload();
  }

  useEffect(async () => {
    const countUsers = await getUsersLength();
    // const countMessages = await getMessagesLength();
    setUserCount(countUsers);
    // setMessageCount(countMessages);
  }, [eventEmitHeader])

  return (
    <header className="header">
      <div className="header-title"> My Chat</div>
      <div className="header-users-count">
        {/*{idAdmin ? <Link to="/users"> {userCount} participants </Link> : <div> {userCount} participants </div>}*/}
        <Link to="/users"> {userCount} participants </Link>
      </div>
      <div className="header-messages-count">
        <Link to="/"> {messageCount} messages </Link>
      </div>
      <div className="header-switch-theme" onClick={onToggleTheme}>
        {toggleTheme ? <i className="far fa-moon"/> : <i className="fas fa-moon"/>}
      </div>
      {lastMessageTime && <div className="header-last-message-date"> last message at {lastMessageTime} </div>}
      <div className="header-logout" onClick={onLogout}><i className="fas fa-sign-out-alt"/></div>
    </header>
  )
}

Header.propTypes = {
    userCount:  PropTypes.number,
    messageCount:  PropTypes.number,
    lastMessageTime:  PropTypes.string,
    toggleTheme:  PropTypes.bool,
    onToggleTheme:  PropTypes.func
};

export default Header;
