import styles from './own-message.css';
import Date from "../../common/date";
import PropTypes from "prop-types";
import Message from "../message/message";

const OwnMessage = ({message, index, deleteMessage, editMessage}) => {
  const date = Date(message.editedAt || message.createdAt)

  return (
    <div className="msg own-message">
      <div className="message-user-avatar">
        <img
          src={message.avatar}
          alt="profile-pic" />
      </div>

      <div className="message-block">
        <div className="message-info">
          <div className="message-user-name">{message.user}</div>
          <div className="message-time">{date}</div>
        </div>

        <div className="message-text">
          {message.text}
        </div>

        <div className="message-setting">
          <div className="message-edit" onClick={(e) => editMessage(e, message.id, message.text)}>
            <i className="fas fa-pen" />
          </div>
          <div className="message-delete" onClick={(e) => deleteMessage(e, message.id)}>
            <i className="far fa-trash-alt" />
          </div>
        </div>
      </div>
    </div>
  )
}

Message.propTypes = {
  id: PropTypes.number,
  user: PropTypes.string,
  avatar: PropTypes.string,
  userId: PropTypes.number,
  text: PropTypes.string,
  createdAt: PropTypes.string,
  editedAt: PropTypes.string
};

export default OwnMessage;
