import React, {useEffect, useState} from "react";
import {Header} from "../common";
// import {addUser, deleteUser, getUsers, updateUser} from "../../services/userServices";

import style from "./users.css"
import useInputState from "../../common/useInputState";
import {createUser, deleteUser, getUsers, getUsersLength, updateUser} from "../../services/domainRequest/usersRequest";

const Users = () => {
  // todo: выводить ошибки и показывать лоадер
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const {value, reset, onChange} = useInputState();
  const [users, setUsers] = useState([]);
  const [usersLocal, setUsersLocal] = useState();
  const [eventEmitHeader, setEventEmitHeader] = useState(false);

  useEffect(async () => {
    const data = await getUsers();
    setUsers(data);

    setEventEmitHeader(!eventEmitHeader);
  }, [usersLocal])

  const handleSubmit = async () => {
    const newUser = {
      username: 'pavel',
      password: 'pawel'
    }
    try {
      const data = await createUser(newUser);
      setUsersLocal(data);
    } catch (error) {
      setError(error);
    }
  };

  const handleUpdate = async (e, id) => {
    e.preventDefault();
    // todo: редактировать инпут
    try {
      const users = await updateUser(id, {username: 'test'});
      setUsersLocal(users);
    } catch (error) {
      setError(error);
    }
  }

  const handleDelete = async (e, id) => {
    e.preventDefault();
    try {
      const users = await deleteUser(id);
      setUsersLocal(users);
    } catch (error) {
      setError(error);
    }
  }

  return (
    <div className="users-component">
      <Header eventEmitHeader={eventEmitHeader}/>
      <div>
        <button className="message-input-button btn-add-user" onClick={() => handleSubmit()}> Add user</button>
        {users.map(user => (
          <div className="user-container" key={user.id}>
            <form className="message-input">
              <input className="message-input-text" type="text" style={{flex: '0', width: '50px'}}
                     defaultValue={user.id}
              />
              <input className="message-input-text" type="text"
                // defaultValue={user.username}
                     onChange={onChange}
                     value={user.username}
              />
              <button className="message-input-button btn-edit-user"
                      onClick={(e) => handleUpdate(e, user.id)}> Edit
              </button>
              <button className="message-input-button btn-delete-user"
                      onClick={(e) => handleDelete(e, user.id)}> Delete
              </button>
            </form>
          </div>
        ))}
      </div>
    </div>
  )
}

export default Users;
