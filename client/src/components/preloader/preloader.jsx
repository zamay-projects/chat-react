import styles from './preloader.css';

const Preloader = () => (
  <div className="preloader">
    <i className="fas fa-redo  fa-spin"/>
  </div>
)

export default Preloader;
