import {Header, MessageInput, MessageList, Modal} from "../common";
import React, {useEffect, useState} from "react";
import * as PropTypes from "prop-types";
import styles from './chat.css';
import {createMessage, deleteMessage, getMessages, updateMessage} from "../../services/domainRequest/messagesRequest";
import {getLoginSession} from "../../services/authService";


const Chat = () => {
  const [isLoaded, setIsLoaded] = useState(false);
  const [messages, setMessages] = useState([]);
  const [messagesLocal, setMessagesLocal] = useState([]);
  const [lastMessageTime, setLastMessageTime] = useState(null);
  const [toggleTheme, setToggleTheme] = useState(true);
  const [openModal, setOpenModal] = useState(false);
  const [idMessage, setIdMessage] = useState(null);
  const [textMessage, setTextMessage] = useState(null);
  const [eventEmitHeader, setEventEmitHeader] = useState(false);

  const getLastMsgTime = (messages) => {
    const sentMsg = new Date(messages[messages.length - 1].createdAt)
    const getYear = sentMsg.getFullYear();
    const getYearNow = new Date().getFullYear();

    return sentMsg.toLocaleString('ru-RU',
      (getYear < getYearNow ? {
        year: 'numeric',
        month: '2-digit',
        day: '2-digit',
        hour: '2-digit', hour12: false, minute: '2-digit'
      } : {
        day: '2-digit',
        month: 'long',
        hour: '2-digit', hour12: false, minute: '2-digit'
      })
    )
  }

  const onToggleTheme = () => {
    setToggleTheme(!toggleTheme);
  }

  const templateMessage = (text, id) => {
    const user = getLoginSession();
    const idMsg = id || '_' + Math.random().toString(36).substr(2, 9);
    const date = new Date();
    return {
      "id": idMsg,
      "userId": user.id,
      "avatar": "https://study.binary-studio.com/uploads/user-data/0a539876bcc60e7e4ad00ab82d2ce100f36f0ab8937739a3a682173b640a07fb4cef907f/c02e9bc5313f33b6c15b1642014376017",
      "user": user.username,
      "text": text,
      "createdAt": date,
      "editedAt": ""
    }
  }

  const handleUpdate = async (text) => {
    try {
      const editMsg = templateMessage(text, idMessage);
      const data = await updateMessage(idMessage, editMsg);
      // messages.find(el => el.id === idMessage).text = text;
      setMessagesLocal(data);
      setOpenModal(!openModal)
    } catch (error){
      console.log(error);
    }
  }

  const onEditMessage = (e, id, text) => {
    e.preventDefault();
    setTextMessage(text);
    setIdMessage(id);
    setOpenModal(!openModal);
  }

  const handleSubmit = async (text) => {
    try {
      const newMsg = templateMessage(text);
      const data = await createMessage(newMsg)
      setMessagesLocal(data);
    } catch (error) {
      console.log(error);
    }
  };

  const handleDelete = async (e, id) => {
    e.preventDefault();
    try {
      const users = await deleteMessage(id);
      setMessagesLocal(users);
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(async () => {
    const data = await getMessages()
    setMessages(data);
    setLastMessageTime(getLastMsgTime(data));
    setEventEmitHeader(!eventEmitHeader);
  }, [messagesLocal])

  return (
    <section className={toggleTheme ? "chat" : "chat dark"}>
      <Header eventEmitHeader={eventEmitHeader}
              lastMessageTime={lastMessageTime}
              onToggleTheme={onToggleTheme}
              toggleTheme={toggleTheme}
      />
      <Modal textMessage={textMessage} handleUpdate={handleUpdate} toggleModal={openModal}/>
      <MessageList isLoaded={isLoaded}
                   messages={messages}
                   deleteMessage={handleDelete}
                   editMessage={onEditMessage}/>
      <MessageInput inputValue={handleSubmit}/>
    </section>
  )
}

Chat.propTypes = {
  url: PropTypes.string
};

export default Chat;
