import style from './login.css';
import {useState} from 'react';
import PropTypes from 'prop-types';
import useToken from "../../services/useToken";
import {SkyClass} from "../sky/sky-class";
import {login} from "../../services/domainRequest/auth";
import {setLoginSession} from "../../services/authService";

const Login = ({ setUserIsLogged }) => {
  const { setToken } = useToken();
  const [username, setUserName] = useState();
  const [password, setPassword] = useState();
  const [rememberMe, setRememberMe] = useState(false);

  function onClickRememberMe() {
    setRememberMe(!rememberMe);
  }

  const handleSubmit = async e => {
    const token = await login({
      username,
      password
    });

    setUserIsLogged(token.result);

    if (rememberMe) {
      setLoginSession(token);
      setToken(token);
    }
  }

  return (
    <>
      <SkyClass />
      <div className="login-form">
        <h2>Welcome to "My Chat"</h2>
        <form onSubmit={handleSubmit}>
          <input type="text" title="username" placeholder="username" onChange={e => setUserName(e.target.value)}/>
          <input type="password" title="password" placeholder="password" onChange={e => setPassword(e.target.value)} />

          <label className="checkbox">
            <input type="checkbox" onClick={onClickRememberMe} id="rememberMe" name="rememberMe" value="remember-me"/>
            Remember me
          </label>
          <button type="submit" className="btn">Login</button>
        </form>
      </div>
    </>
  )
}

Login.propTypes = {
  setToken: PropTypes.func.isRequired
}

export default Login;
